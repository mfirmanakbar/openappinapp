package com.firmanpro.openappinapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private String open_apps = "com.firmanpro.awesome";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }

    public void onClickOpen(View view){
        Intent launchIntent = getPackageManager().getLaunchIntentForPackage(open_apps);
        if (launchIntent != null) {
            startActivity(launchIntent);//null pointer check in case package name was not found
        }else {
            Toast.makeText(getApplicationContext(),"no apps with package "+open_apps,Toast.LENGTH_SHORT).show();
        }
    }

}
